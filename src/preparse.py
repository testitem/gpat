import glob
import json
import base64
import subprocess


def bees(DIR):
  re = {}

  print("extracting...")

  subprocess.run(["tar", "-xjf", f"raw/{DIR}.tar.bz2", "--exclude", "*.log"])

  print("reading...")

  for i in glob.glob(f"{DIR}/*.json"):
    re[int(i.split("/")[1][:-5])] = json.load(open(i))

  print("writing...")

  json.dump(re, open(f"data/{DIR}.json", "w"))

  print("cleaning up... ")

  subprocess.run(["rm", "-r", f"{DIR}"])

  print(f"{DIR} done")


APPNAMES = """accuweather
autoscout24
dictionarymerriamwebster
duolingo
flipboard
linewebtoon
nikerunclub
onenote
quizlet
spotify
tripadvisor
trivago
wattpadfreebooks
wish
youtube
zedge""".split("\n")
TESTINGTOOLS = ["ape", "monkey", "wctester"]

for i in range(1, 7):
  for app in APPNAMES:
    for tt in TESTINGTOOLS:
      DIR = f"{tt}-{app}-{i}"
      bees(DIR)
