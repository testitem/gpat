import pickle
import json
import sim

APPNAMES = """accuweather
autoscout24
dictionarymerriamwebster
duolingo
flipboard
linewebtoon
nikerunclub
onenote
quizlet
spotify
tripadvisor
trivago
wattpadfreebooks
wish
youtube
zedge""".split("\n")
TESTINGTOOLS = ["ape", "monkey", "wctester"]
mm = {}
for x in APPNAMES:
  for tt in TESTINGTOOLS:
    PFX = f"{tt}-{x}"
    for y in range(1, 7):
      DIR = f"{PFX}-{y}"
      print("loading",DIR)
      j = json.load(open(f"data/{DIR}.json"))
      for i in j:
        j[i] = {"act_id":j[i]["act_id"], "hash":sim.hash_layout(j[i])}
      mm[DIR] = j
pickle.dump(mm, open("dumpfile.pickle","wb"))