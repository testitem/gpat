import os
import sys
import pickle

exclude_first_ts = [int(t) for t in os.environ.get('EXCLUDE_FIRST', "").strip().split("\n") if t]
# print("exclude_first_ts = %d" % exclude_first_ts)

list_output_file = os.environ.get('LIST_OUT', "")
timed_list_output_file = "mtds.pickle"
count_output_file = os.environ.get('COUNT_OUT', "")
use_inst_cov = os.environ.get('USE_INST_COV')

DOWNSAMPLING_INTERVAL = int(os.environ.get('DOWNSAMPLING_INTERVAL', "15"))

mtds = {}
# mtds = set()
mtds_ds, ts_last_ds = None, 0
mtds_first = None
ts_first = None
mtds_exclude = set()
mtds_by_time = []


def flush_ds():
  global mtds_ds, mtds
  if mtds_ds:
    for mtd in mtds_ds:
      mtds[mtd] = mtds.get(mtd, 0) + 1
  mtds_ds = set()


for fp in sys.argv[1:]: # all files in coverage # python parse-minitrace.py minitrace/cov-*.log
  print(fp)
  curr_mtds = None
  curr_uniq_mtds = set()
  try:
    pos1 = fp.index('cov-') + 4
    pos2 = fp.index('.log', pos1)
    ts = int(fp[pos1:pos2])
    if exclude_first_ts:
      if ts in exclude_first_ts: # TODO: and not ts_first:
        curr_mtds = set()
    elif not ts_first or ts_first > ts:
      curr_mtds = set()
  except ValueError:
    print('Unsupported path: ' + fp)
    sys.exit(-1)
  if ts - ts_last_ds >= DOWNSAMPLING_INTERVAL:
    flush_ds()
    ts_last_ds = ts
  def report(mtd_sig):
    if mtd_sig not in mtds_ds:
      mtds_ds.add(mtd_sig)
      if mtd_sig not in mtds:
        curr_uniq_mtds.add(mtd_sig)
    #   mtds[mtd_sig] = 1
    # else:
    #   mtds[mtd_sig] += 1
    if curr_mtds is not None:
      # TODO: mtds_exclude.add(mtd_sig)
      curr_mtds.add(mtd_sig)
  with open(fp) as f:
    lines = f.read().strip().splitlines()
    for line in lines:
      line = line.strip()
      if line[0:2] != '0x':
        continue
      # 0x7fb8e7f840c0  com.facebook.FacebookRequestError$b     <init>  ()V     SourceFile      12      100101010101
      segs = line.split()
      mtd_sig = '/'.join(segs[1:4])
      if use_inst_cov:
        for i, c in enumerate(segs[-1]):
          if c == '1':
            report(mtd_sig + ':' + str(i))
      else:
        report(mtd_sig)
      # inst_mask = list(segs[-1])
      # mask = mtds.get(mtd_sig)
      # if mask:
      #   if len(mask) != len(inst_mask):
      #     print('inst mask length unmatch for ' + mtd_sig)
      #     sys.exit(-1)
      # else:
      #   mask = ['0'] * len(inst_mask)
      # for i in range(0, len(mask)):
      #   if mask[i] == '0' and inst_mask[i] == '1':
      #     mask[i] = '1'
      # mtds[mtd_sig] = mask
  if ts_first: # and curr_uniq_mtds:
    mtds_by_time.append((ts - ts_first, curr_uniq_mtds)) # approximation
  if curr_mtds: # TODO
    ts_first = ts # TODO
    mtds_first = curr_mtds # TODO
    print(fp)
flush_ds()

print('|methods|', len(mtds))
print('|init_methods|', len(mtds_first))
incr_mtds = set(mtds).difference(mtds_first)
print('|methods - init_methods|', len(incr_mtds))
# TODO: print('|excl_methods|', len(mtds_exclude))
# TODO: print('|methods - excl_methods|', len(mtds.difference(mtds_exclude)))

ct_sum = 0
for mtd in mtds_first:
  ct_sum += mtds[mtd]
  mtds[mtd] -= 1
  if mtds[mtd] <= 0: del mtds[mtd]
print('|methods_ct - init_methods_ct|', len(mtds))
print('ct_sum', ct_sum)

if list_output_file:
  print('writing to ' + list_output_file)
  with open(list_output_file, 'wb') as handle:
    pickle.dump(incr_mtds, handle)

if timed_list_output_file:
  print('writing to ' + timed_list_output_file)
  with open(timed_list_output_file, 'wb') as handle:
    pickle.dump(mtds_by_time, handle)

if count_output_file:
  print('writing to ' + count_output_file)
  with open(count_output_file, 'wb') as handle:
    pickle.dump(mtds, handle)
