import os

for i in os.listdir("img"):
  x = os.path.abspath(f"img/{i}")
  print(x)
  y = os.path.abspath(f"pdf/{i[:-4]}.pdf")
  os.system(f'inkscape --export-filename="{y}" "{x}"')