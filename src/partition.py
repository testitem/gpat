import networkx as nx
import pydot
import metis
import pydot
import pickle

import sim

FMT = "svg"


def genGraph(edgelist: list[tuple]):
  G: nx.DiGraph = nx.DiGraph()
  G.graph['edge_weight_attr'] = 'weight'
  for u, v in edgelist:
    if G.has_edge(u, v):
      G[u][v]["weight"] += 1
    else:
      G.add_edge(u, v, weight=1)
  return G


def partialpfxelim(labels: set):
  labels = set(labels)
  pfx = ""
  for i in range(1, max(map(len, labels))):
    pfxs = {}
    for _ in labels:
      x = _
      if x[:i] not in pfxs:
        pfxs[x[:i]] = 0
      pfxs[x[:i]] += 1
    bc, be = max((pfxs[x], x) for x in pfxs)
    if bc > len(labels) - bc:
      pfx = be
  ans = {}
  for x in labels:
    if x[:len(pfx)] == pfx:
      ans[x] = x[len(pfx):]
    else:
      ans[x] = x
  return ans

def pfxelim(labels: set):
  labels = set(labels)
  pfx = ""
  for i in range(1, max(map(len, labels))):
    pfxs = {}
    for _ in labels:
      x = _
      if x[:i] not in pfxs:
        pfxs[x[:i]] = 0
      pfxs[x[:i]] += 1
    bc, be = max((pfxs[x], x) for x in pfxs)
    if bc > len(labels) - bc:
      pfx = be
  def gen(x):
    if x[:len(pfx)] == pfx:
      x = x[len(pfx):]
    res = [""]
    for i in x:
      if i.isupper() and res != [""]:
        res += [""]
      res[-1] += i
    return tuple(res)
  dislab = {}
  tr = 0
  for i in labels:
    dislab[i] = gen(i)
    tr = max(tr,max(len(s) for s in dislab[i]))
  while tr-1:
    idx = 0
    while 1:
      cnt = 0
      alt = dict(dislab)
      for i in dislab:
        if idx < len(dislab[i]):
          cnt += 1
          if len(dislab[i][idx]) == tr:
            f = list(dislab[i])
            f[idx]=f[idx][:tr-1]
            alt[i] = tuple(f)
      if cnt == 0:
        break
      pop = {}
      for i in alt:
        pop[alt[i]] = 0
      for i in alt:
        pop[alt[i]] += 1
      for i in dislab:
        if pop[alt[i]] == 1:
          dislab[i]=alt[i]
      idx +=1
    tr -= 1
  sf = {x:"".join(dislab[x]) for x in dislab}
  
  return sf

def get_big_graph(j: dict, fname, legend):
  k = sorted(map(int, j.keys()))
  def identify(dct): return (dct["hash"], dct["act_id"])
  # identify = lambda dct: (dct["hash"], dct["act_id"])
  cnt = 0
  lis = []
  gr = {}
  labels = {}
  for i in k:
    lis.append(identify(j[str(i)]))
    if lis[-1] not in gr:
      nex = len(gr)
      labels[nex] = lis[-1][1]
      gr[lis[-1]] = nex
  G: nx.DiGraph = genGraph([(gr[lis[i]], gr[lis[i + 1]])
                           for i in range(len(lis) - 1)])
  # labels, legend = pfxelim(labels)
  labels = {i: legend[labels[i]] for i in labels}
  for i in G.nodes:
    G.nodes[i]["xlabel"] = labels[i]
  return G,None


def get_small_graph(j: dict, legend):
  k = sorted(map(int, j.keys()))
  def identify(dct): return (dct["act_id"])
  cnt = 0
  lis = []
  gr = {}
  labels = {}
  for i in k:
    lis.append(j[str(i)]["act_id"])
    if lis[-1] not in gr:
      nex = len(gr)
      labels[nex] = lis[-1]
      gr[lis[-1]] = nex
  # labels, legend = pfxelim(labels)
  labels = {i: legend[labels[i]] for i in labels}
  G = genGraph([(gr[lis[i]], gr[lis[i + 1]]) for i in range(len(lis) - 1)])
  return (G, labels)


def get_multi_small_graph(js: list[dict], legend):
  gr = {}
  labels = {}
  lises = []
  for j in js:
    k = list(map(str, sorted(map(int, j.keys()))))
    # print("Fn",j[k[0]]["act_id"],j[k[0]]["hash"])
    lis = []
    for i in k:
      lis.append(j[i]["act_id"])
      if lis[-1] not in gr:
        nex = len(gr)
        labels[nex] = lis[-1]
        gr[lis[-1]] = nex
    lises += [lis]
  # labels, legend = pfxelim(labels)
  labels = {i: legend[labels[i]] for i in labels}

  G = genGraph([(gr[lis[i]], gr[lis[i + 1]])
               for lis in lises for i in range(len(lis) - 1)])
  return (G, labels)


def ggggolor(G: nx.DiGraph):
  G2 = nx.DiGraph()
  def func(u, v, dc): return 1 / dc["weight"]
  ff: dict = nx.shortest_path(G, 0, weight=func)  # type: ignore
  if 0 in G.nodes and "xlabel" in G.nodes[0]:
    for i in ff:
      if len(ff[i]) >= 2:
        G2.add_edge(ff[i][-2], ff[i][-1])
        G2.nodes[ff[i][-2]]["xlabel"] = G.nodes[ff[i][-2]]["xlabel"]
        G2.nodes[ff[i][-1]]["xlabel"] = G.nodes[ff[i][-1]]["xlabel"]
    for i in ff:
      if len(ff[i]) >= 2:
        if G2.nodes[ff[i][-2]]["xlabel"] in [G2.nodes[ff[i][-1]]["xlabel"], G2.nodes[ff[i][-1]]["xlabel"] + "_DELETE"]:
          G2.nodes[ff[i][-1]]["xlabel"] += "_DELETE"
    for x in G2.nodes:
      if G2.nodes[x]["xlabel"].endswith("_DELETE"):
        del G2.nodes[x]["xlabel"]
  else:
    for i in G.nodes:
      G2.add_node(i)
    for i in ff:
      if len(ff[i]) >= 2:
        G2.add_edge(ff[i][-2], ff[i][-1])
  Gx = nx.DiGraph(G)
  for u, v in Gx.edges:
    # Gx[u][v]["weight"] = int(1000000/Gx[u][v]["weight"])
    Gx[u][v]["weight"] = 1
  Gm = metis.networkx_to_metis(Gx)
  (cut, parts) = metis.part_graph(Gm, 6)
  colors = ['red', 'blue', '#b5e51d', 'cyan', 'magenta', '#fea200']
  for i, part in enumerate(parts):
    G.nodes[i]['color'] = colors[part]
    G2.nodes[i]['color'] = colors[part]
    G.nodes[i]['node_value'] = part
  return (G, G2)


def checknum(G: nx.DiGraph):
  ans1, ans2 = 0, 0
  for u, v in G.edges:
    if G.nodes[u]["node_value"] == G.nodes[v]["node_value"]:
      ans1 += G[u][v]["weight"]
      ans2 += G[u][v]["weight"]**2
  return (ans1, ans2)

def wtgr(gdot: pydot.Dot, fname):
  gdot.set("forcelabels", True)
  gdot.set("layout", "twopi")
  gdot.set("overlap_scaling", 10)
  gdot.set("ranksep", 10)
  gdot.set("bgcolor","transparent")
  for i in gdot.get_edge_list():
    i.set("color", "#00000050")
  print("Rendering", fname)
  # gdot.write(fname + ".dot", format="dot")
  gdot.write(fname, format=FMT)


ALL = ["accuweather", "autoscout24", "duolingo", "onenote", "wattpadfreebooks"]
WATTPAD = ["wattpadfreebooks"]

APPNAMES = """accuweather
autoscout24
dictionarymerriamwebster
duolingo
flipboard
linewebtoon
nikerunclub
onenote
quizlet
spotify
tripadvisor
trivago
wattpadfreebooks
wish
youtube
zedge""".split("\n")
APPNAMES = """youtube
zedge""".split("\n")
TESTINGTOOLS = ["ape", "monkey", "wctester"]
mm = pickle.load(open("dumpfile.pickle","rb"))
for x in APPNAMES:
  actvs = []
  for tt in TESTINGTOOLS:
    js = []
    PFX = f"{tt}-{x}"
    for y in range(1, 7):
      DIR = f"{PFX}-{y}"
      j = mm[DIR]
      actvs += [j[t]["act_id"] for t in j]
  legend = pfxelim(set(actvs))
  dt:dict[str,list] = {i:[None]*7 for i in TESTINGTOOLS}
  for tt in TESTINGTOOLS:
    PFX = f"{tt}-{x}"
    js = []
    for y in range(1,7):
      DIR = f"{PFX}-{y}"
      j = mm[DIR]
      js += [j]
      try:
        G, tx = get_big_graph(j, DIR, legend)
        (G, G2) = ggggolor(G)
        gdot = nx.nx_pydot.to_pydot(G2)
        gdot.set("overlap", "prism0")
        for i in gdot.get_node_list():
          i.set("label", "")
          i.set("shape", "point")
        (Gr, labels) = get_small_graph(j, legend)
        (Gr, Gr2) = ggggolor(Gr)
        Gr2 = nx.relabel_nodes(Gr2, labels)
        gdot2 = nx.nx_pydot.to_pydot(Gr2)
        gdot2.set("overlap", "prism20")
        Gr3:nx.DiGraph = nx.relabel_nodes(Gr, labels)
        gdot3 = nx.nx_pydot.to_pydot(Gr3)
        gdot3.set("overlap", "prism20")
        wtgr(gdot, f"img/{DIR}-L.{FMT}")
        wtgr(gdot2, f"img/{DIR}-S.{FMT}")
        wtgr(gdot3, f"img/{DIR}-SF.{FMT}")
      except Exception as e:
        import traceback
        traceback.print_exc()
        print("fail", DIR)
        pass
    # generate_legend_cumulative(legend, PFX, js)
    (Gr, labels) = get_multi_small_graph(js, legend)
    (Gr, Gr2) = ggggolor(Gr)
    Gr2 = nx.relabel_nodes(Gr2, labels)
    gdot2 = nx.nx_pydot.to_pydot(Gr2)
    gdot2.set("overlap", "prism20")
    Gr3: nx.DiGraph = nx.relabel_nodes(Gr, labels)
    gdot3 = nx.nx_pydot.to_pydot(Gr3)
    gdot3.set("overlap", "prism20")
    gdot3.set("concentrate", "true")
    wtgr(gdot2, f"img/{PFX}-S.{FMT}")
    wtgr(gdot3, f"img/{PFX}-SF.{FMT}")
    # print(checknum(Gr))