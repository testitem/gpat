import networkx as nx
import pydot
import metis
import pydot
import json

import sim


def genGraph(edgelist: list[tuple]):
  G: nx.DiGraph = nx.DiGraph()
  G.graph['edge_weight_attr'] = 'weight'
  for u, v in edgelist:
    if G.has_edge(u, v):
      G[u][v]["weight"] += 1
    else:
      G.add_edge(u, v, weight=1)
  return G


def pfxelim(labels: dict):
  labels = dict(labels)
  pfx = ""
  for i in range(1, max(map(len, labels.values()))):
    pfxs = {}
    for _ in labels:
      x = labels[_]
      if x[:i] not in pfxs:
        pfxs[x[:i]] = 0
      pfxs[x[:i]] += 1
    bc, be = max((pfxs[x], x) for x in pfxs)
    if bc > len(labels) - bc:
      pfx = be
  for i in labels:
    if labels[i][:len(pfx)] == pfx:
      labels[i] = labels[i][len(pfx):]
  return labels

def heavyelim(labels: dict):
  def gen(x):
    res = [""]
    for i in x:
      if i.isupper() and res != [""]:
        res += [""]
      res[-1] += i
    return tuple(res)
  dislab = {}
  tr = 0
  for i in labels:
    dislab[labels[i]] = gen(labels[i])
    tr = max(tr,max(len(s) for s in dislab[labels[i]]))
  while tr-1:
    idx = 0
    while 1:
      cnt = 0
      alt = dict(dislab)
      for i in dislab:
        if idx < len(dislab[i]):
          cnt += 1
          if len(dislab[i][idx]) == tr:
            f = list(dislab[i])
            f[idx]=f[idx][:tr-1]
            alt[i] = tuple(f)
      if cnt == 0:
        break
      pop = {}
      for i in alt:
        pop[alt[i]] = 0
      for i in alt:
        pop[alt[i]] += 1
      for i in dislab:
        if pop[alt[i]] == 1:
          dislab[i]=alt[i]
      idx +=1
    tr -= 1
  sf = {x:"".join(dislab[x]) for x in dislab}
  
  return ({i:sf[labels[i]] for i in labels},sf)

def generate_legend(legend, fname):
  gdot = pydot.Dot(graph_type="digraph")
  gdot.obj_dict['attributes']["nodesep"] = 0
  gdot.obj_dict['attributes']["ranksep"] = 1
  gdot.obj_dict['attributes']["rankdir"] = "LR"

  # cluster = pydot.Cluster(label = "Legend")
  # keys = sorted(i for i in legend)
  # c = len(keys)
  # v1 = """<<table border="0" cellpadding="2" cellspacing="0" cellborder="0">\n"""
  # v2 = """<<table border="0" cellpadding="2" cellspacing="0" cellborder="0">\n"""
  # for i in range(c):
  #   k = keys[i]
  #   v = legend[k]
  #   v1 += f"""<tr><td align="right" port="i{i}">{k}</td></tr>\n"""
  #   v2 += f"""<tr><td port="i{i}">{v}</td></tr>\n"""
  # v1 += "</table>>"
  # v2 += "</table>>"
  # print(v1)
  # print(v2)
  # node_f1 = pydot.Node("A",label=v1,shape="plaintext")
  # node_f1.obj_dict["attributes"]["label"]=v1
  # print(node_f1)
  # node_f2 = pydot.Node("B",label=v2,shape="plaintext")
  # node_f2.obj_dict["attributes"]["label"]=v2
  # cluster.add_node(node_f1)
  # cluster.add_node(node_f2)
  # for i in range(c):
  #   n1 = pydot.Node(f"A:i{i}:e")
  #   n2 = pydot.Node(f"B:i{i}:w")
  #   gdot.add_edge(pydot.Edge(n1, n2))
  eds = []
  for _,i in sorted((legend[a],a) for a in legend)[::-1]:
    node_foo = pydot.Node(f"{i}_1",label=i,shape="plaintext")
    gdot.add_node(node_foo)
    node_foo2 = pydot.Node(f"{i}_2",label=legend[i],shape="plaintext")
    gdot.add_node(node_foo2)
    eds += [pydot.Edge(node_foo, node_foo2)]
  for x in eds:
    gdot.add_edge(x)
  gdot.obj_dict['attributes']["label"] = "Legend"
  gdot.write(f"img/legend-{fname}.png", format="png")

def get_big_graph(j: dict, fname):
  k = sorted(map(int, j.keys()))
  def identify(dct): return (sim.hash_layout(dct), dct["act_id"])
  # identify = lambda dct: (dct["hash"], dct["act_id"])
  cnt = 0
  lis = []
  gr = {}
  labels = {}
  for i in k:
    lis.append(identify(j[str(i)]))
    if lis[-1] not in gr:
      nex = len(gr)
      labels[nex] = lis[-1][1]
      gr[lis[-1]] = nex
  G: nx.DiGraph = genGraph([(gr[lis[i]], gr[lis[i + 1]])
                           for i in range(len(lis) - 1)])
  labels, legend = heavyelim(pfxelim(labels))
  generate_legend(legend, fname)
  for i in G.nodes:
    G.nodes[i]["xlabel"] = labels[i]
  return G


def get_small_graph(j: dict):
  k = sorted(map(int, j.keys()))
  def identify(dct): return (dct["act_id"])
  cnt = 0
  lis = []
  gr = {}
  labels = {}
  for i in k:
    lis.append(j[str(i)]["act_id"])
    if lis[-1] not in gr:
      nex = len(gr)
      labels[nex] = lis[-1]
      gr[lis[-1]] = nex
  labels = pfxelim(labels)
  G = genGraph([(gr[lis[i]], gr[lis[i + 1]]) for i in range(len(lis) - 1)])
  return (G, labels)


def get_multi_small_graph(js: list[dict]):
  gr = {}
  labels = {}
  lises = []
  for j in js:
    k = list(map(str, sorted(map(int, j.keys()))))
    # print("Fn",j[k[0]]["act_id"],j[k[0]]["hash"])
    lis = []
    for i in k:
      lis.append(j[i]["act_id"])
      if lis[-1] not in gr:
        nex = len(gr)
        labels[nex] = lis[-1]
        gr[lis[-1]] = nex
    lises += [lis]
  labels = pfxelim(labels)
  G = genGraph([(gr[lis[i]], gr[lis[i + 1]])
               for lis in lises for i in range(len(lis) - 1)])
  return (G, labels)


def ggggolor(G: nx.DiGraph):
  G2 = nx.DiGraph()
  def func(u, v, dc): return 1 / dc["weight"]
  ff: dict = nx.shortest_path(G, 0, weight=func)  # type: ignore
  if 0 in G.nodes and "xlabel" in G.nodes[0]:
    print(G.nodes[0])
    for i in ff:
      if len(ff[i]) >= 2:
        G2.add_edge(ff[i][-2], ff[i][-1])
        G2.nodes[ff[i][-2]]["xlabel"] = G.nodes[ff[i][-2]]["xlabel"]
        G2.nodes[ff[i][-1]]["xlabel"] = G.nodes[ff[i][-1]]["xlabel"]
    for i in ff:
      if len(ff[i]) >= 2:
        if G2.nodes[ff[i][-2]]["xlabel"] in [G2.nodes[ff[i][-1]]["xlabel"], G2.nodes[ff[i][-1]]["xlabel"] + "_DELETE"]:
          G2.nodes[ff[i][-1]]["xlabel"] += "_DELETE"
    for x in G2.nodes:
      if G2.nodes[x]["xlabel"].endswith("_DELETE"):
        del G2.nodes[x]["xlabel"]
  else:
    for i in ff:
      if len(ff[i]) >= 2:
        G2.add_edge(ff[i][-2], ff[i][-1])
  Gx = nx.DiGraph(G)
  for u, v in Gx.edges:
    # Gx[u][v]["weight"] = int(1000000/Gx[u][v]["weight"])
    Gx[u][v]["weight"] = 1
  Gm = metis.networkx_to_metis(Gx)
  (cut, parts) = metis.part_graph(Gm, 6)
  colors = ['red', 'blue', 'green', 'cyan', 'magenta', 'yellow']
  for i, part in enumerate(parts):
    G.nodes[i]['color'] = colors[part]
    G2.nodes[i]['color'] = colors[part]
    G.nodes[i]['node_value'] = part
  return (G, G2)


def checknum(G: nx.DiGraph):
  ans1, ans2 = 0, 0
  for u, v in G.edges:
    if G.nodes[u]["node_value"] == G.nodes[v]["node_value"]:
      ans1 += G[u][v]["weight"]
      ans2 += G[u][v]["weight"]**2
  return (ans1, ans2)



def wtgr(gdot: pydot.Dot, fname):
  gdot.set("layout", "twopi")
  gdot.set("overlap_scaling", 10)
  gdot.set("ranksep", 10)
  print("Rendering", fname)
  # gdot.write(fname + ".dot", format="dot")
  gdot.write(fname, format="png")


ALL = ["accuweather", "autoscout24", "duolingo", "onenote", "wattpadfreebooks"]
WATTPAD = ["wattpadfreebooks"]

APPNAMES = """accuweather
autoscout24
dictionarymerriamwebster
duolingo
flipboard
linewebtoon
nikerunclub
onenote
quizlet
spotify
tripadvisor
trivago
wattpadfreebooks
wish
youtube
zedge""".split("\n")
TESTINGTOOLS = ["ape", "monkey", "wctester"]

for tt in TESTINGTOOLS:
  for x in APPNAMES:
    js = []
    PFX = f"{tt}-{x}"
    f = open(f"rep/{PFX}","w")
    tlogt = {}
    cntt = {}
    for y in range(1, 7):
      tlog = {}
      cnt = {}
      DIR = f"{PFX}-{y}"
      j = json.load(open(f"data/{DIR}.json"))
      ts = sorted(j)
      print(PFX,(int(ts[-1])-int(ts[0]))/1000)
      for t1, t2 in zip(ts, ts[1:]):
        act = j[t1]["act_id"]
        if act not in cnt:
          cnt[act] = tlog[act] = 0
        cnt[act] += 1
        tlog[act] += int(t2) - int(t1)
      print(y,file = f)
      for i in sorted(tlog):
        print(i, cnt[i], tlog[i], tlog[i]/cnt[i],file=f)
        if i not in cntt:
          cntt[i] = tlogt[i] = 0
        cntt[i] += cnt[i]
        tlogt[i] += tlog[i]
      print(file=f)
    print("TOTAL", file=f)
    for i in sorted(tlogt):
      print(i, cntt[i], tlogt[i], tlogt[i]/cntt[i],file=f)
    f.close()
    print(PFX)
