import networkx as nx
import pydot
import metis
import scipy.stats
# import json

import sim
import pickle
FMT = "svg"


def genGraph(edgelist: list[tuple]):
  G: nx.DiGraph = nx.DiGraph()
  G.graph['edge_weight_attr'] = 'weight'
  for u, v in edgelist:
    if G.has_edge(u, v):
      G[u][v]["weight"] += 1
    else:
      G.add_edge(u, v, weight=1)
  return G


def partialpfxelim(labels: set):
  labels = set(labels)
  pfx = ""
  for i in range(1, max(map(len, labels))):
    pfxs = {}
    for _ in labels:
      x = _
      if x[:i] not in pfxs:
        pfxs[x[:i]] = 0
      pfxs[x[:i]] += 1
    bc, be = max((pfxs[x], x) for x in pfxs)
    if bc > len(labels) - bc:
      pfx = be
  ans = {}
  for x in labels:
    if x[:len(pfx)] == pfx:
      ans[x] = x[len(pfx):]
    else:
      ans[x] = x
  return ans

def pfxelim(labels: set):
  labels = set(labels)
  pfx = ""
  for i in range(1, max(map(len, labels))):
    pfxs = {}
    for _ in labels:
      x = _
      if x[:i] not in pfxs:
        pfxs[x[:i]] = 0
      pfxs[x[:i]] += 1
    bc, be = max((pfxs[x], x) for x in pfxs)
    if bc > len(labels) - bc:
      pfx = be
  def gen(x):
    if x[:len(pfx)] == pfx:
      x = x[len(pfx):]
    res = [""]
    for i in x:
      if i.isupper() and res != [""]:
        res += [""]
      res[-1] += i
    return tuple(res)
  dislab = {}
  tr = 0
  for i in labels:
    dislab[i] = gen(i)
    tr = max(tr,max(len(s) for s in dislab[i]))
  while tr-1:
    idx = 0
    while 1:
      cnt = 0
      alt = dict(dislab)
      for i in dislab:
        if idx < len(dislab[i]):
          cnt += 1
          if len(dislab[i][idx]) == tr:
            f = list(dislab[i])
            f[idx]=f[idx][:tr-1]
            alt[i] = tuple(f)
      if cnt == 0:
        break
      pop = {}
      for i in alt:
        pop[alt[i]] = 0
      for i in alt:
        pop[alt[i]] += 1
      for i in dislab:
        if pop[alt[i]] == 1:
          dislab[i]=alt[i]
      idx +=1
    tr -= 1
  sf = {x:"".join(dislab[x]) for x in dislab}
  
  return sf

bees = ["%s", "{\\color{red}{\\myuline{%s}}}"]

def generate_legend(legend, fname, j:dict):
  tlog = {i:0 for i in legend}
  cnt = {i:0 for i in legend}
  ts = sorted(map(int,j))
  for t1, t2 in zip(ts, ts[1:]):
    act = j[str(t1)]["act_id"]
    cnt[act] += 1
    tlog[act] += t2 - t1
  peaks1 = [j for i,j in sorted((cnt[k],k) for k in legend)[::-1]][:3]
  cucnt, cutime = sum(cnt[i] for i in legend), sum(tlog[i] for i in legend)
  peaks2 = [j for i,j in sorted((tlog[k]/cnt[k],k) for k in legend if cnt[k])[::-1]][:3]
  peaks3 = [j for i,j in sorted((tlog[k],k) for k in legend)[::-1]][:3]
  dd = partialpfxelim(legend)
  with open(f"tables/legend-{fname}.tex","w") as f:
    print(r"\begin{tabular}{|llll|}",file=f)
    print(r"\hline",file=f)
    print(r"\multicolumn{2}{|l|}{Legend} & \multicolumn{1}{l|}{\# (\%)} & $\overline{\mbox{ms}}$ (\%) \\ \hline",file=f)
    for li, i in sorted((legend[a],a) for a in legend):
      if cnt[i] == 0:
        continue
      x1 = bees[i in peaks1] % ("%d (%.1f" % (cnt[i], cnt[i]/cucnt*100) + ")")
      x2 = bees[i in peaks2] % ("%.0f" % (tlog[i]/cnt[i]))
      x3 = bees[i in peaks3] % ("(%.1f)" % (tlog[i]/cutime*100))
      x23 = bees[i in peaks2 and i in peaks3] % " "
      print("%s & %s & %s & %s%s%s \\\\" % (li, dd[i].replace("_","\\_"), x1, x2, x23, x3),file=f)
    print(r"\hline\end{tabular}",file=f)
  return cutime

def generate_legend_cumulative(legend, fname, js:list[dict]):
  tlog : list[dict] = [{i:0 for i in legend} for j in range(len(js))]
  cnt : list[dict] = [{i:0 for i in legend} for j in range(len(js))]
  for i in range(len(js)):
    j = js[i]
    ts = sorted(map(int,j))
    for t1, t2 in zip(ts, ts[1:]):
      act = j[str(t1)]["act_id"]
      if act not in cnt[i]:
        cnt[i][act] = tlog[i][act] = 0
      cnt[i][act] += 1
      tlog[i][act] += t2 - t1
  peaks1 = [[j for i,j in sorted((cnt[t][k],k) for k in legend)[::-1]][:3] for t in range(len(js))]
  cucnt, cutime = [sum(cnt[t][i] for i in legend) for t in range(len(js))], [sum(tlog[t][i] for i in legend) for t in range(len(js))]
  peaks2 = [[j for i,j in sorted((tlog[t][k]/cnt[t][k],k) for k in legend if cnt[t][k])[::-1]][:3] for t in range(len(js))]
  peaks3 = [[j for i,j in sorted((tlog[t][k],k) for k in legend)[::-1]][:3] for t in range(len(js))]

  d = len(js)
  peaks1T = [j for i,j in sorted((sum(cnt[t][k] for t in range(d)),k) for k in legend)[::-1]][:3]
  peaks2T = [j for i,j in sorted((sum(tlog[t][k] for t in range(d))/sum(cnt[t][k] for t in range(d)),k) for k in legend
  if sum(cnt[t][k] for t in range(d)))[::-1]][:3]
  peaks3T = [j for i,j in sorted((sum(tlog[t][k] for t in range(d)),k) for k in legend)[::-1]][:3]
  dd = partialpfxelim(legend)
  dcc = {}
  with open(f"tables/legend-{fname}.tex","w") as f:
    print(r"\begin{tabular}{|ll|" + "ll|"*(d+1) + "}",file=f)
    print(r"\hline",file=f)
    print(r"\multicolumn{2}{|l|}{Legend}",file=f)
    for i in range(1, d+1):
      print(r"& \multicolumn{2}{l|}{Run","%d (%.1fmin)}"%(i,cutime[i-1]/60000),file=f)
    print(r"& \multicolumn{2}{l|}{Total}",file=f)
    print("\\\\ \\cline{3-%d} &" % (2*d+4),file=f)
    for i in range(1, d+2):
      print(r"& \# (\%) & $\overline{\mbox{ms}}$ (\%)",file=f)
    print(r"\\ \hline",file=f)
    for li, i in sorted((legend[a],a) for a in legend):
      print("%s & %s" % (li, dd[i].replace("_","\\_")),file=f)
      vv = 0
      for x in range(d):
        x1 = bees[i in peaks1[x]] % ("%d (%.1f" % (cnt[x][i], cnt[x][i]/cucnt[x]*100) + ")")
        if cnt[x][i] == 0:
          x1 = r"\textbf{None}"
        x2233 = ""
        if cnt[x][i]:
          vv += 1
          x2 = bees[i in peaks2[x]] % ("%.0f" % (tlog[x][i]/cnt[x][i]))
          x3 = bees[i in peaks3[x]] % ("(%.1f)" % (tlog[x][i]/cutime[x]*100))
          x23 = bees[i in peaks2[x] and i in peaks3[x]] % " "
          x2233 = x2 +  x23 + x3
        print("& %s & %s" % (x1, x2233),file=f)
      x1 = "{\\color{red}{\\textbf{None}}}"
      x2233 = ""
      if sum(cnt[x][i] for x in range(d)):
        x1 = bees[i in peaks1T] % ("%d (%.1f" % (sum(cnt[x][i] for x in range(d)), 
        sum(cnt[x][i] for x in range(d))/sum(cucnt[x] for x in range(d))*100) + ")")
        x2 = bees[i in peaks2T] % ("%.0f" % (sum(tlog[x][i] for x in range(d))/sum(cnt[x][i] for x in range(d))))
        x3 = bees[i in peaks3T] % ("(%.1f)" % (sum(tlog[x][i] for x in range(d))/sum(cutime[x] for x in range(d))*100))
        x23 = bees[i in peaks2T and i in peaks3T] % " "
        x2233 = x2 +  x23 + x3
      dcc[i] = (x1,x2233, str(vv) if vv else "")
      print("& %s & %s \\\\"%(x1,x2233),file=f)
    print(r"\hline\end{tabular}",file=f)
  return dcc

def get_big_graph(j: dict, fname, legend):
  k = sorted(map(int, j.keys()))
  def identify(dct): return (dct["hash"], dct["act_id"])
  # identify = lambda dct: (dct["hash"], dct["act_id"])
  cnt = 0
  lis = []
  gr = {}
  labels = {}
  for i in k:
    lis.append(identify(j[str(i)]))
    if lis[-1] not in gr:
      nex = len(gr)
      labels[nex] = lis[-1][1]
      gr[lis[-1]] = nex
  G: nx.DiGraph = genGraph([(gr[lis[i]], gr[lis[i + 1]])
                           for i in range(len(lis) - 1)])
  # labels, legend = pfxelim(labels)
  labels = {i: legend[labels[i]] for i in labels}
  tx=generate_legend(legend, fname, j)
  for i in G.nodes:
    G.nodes[i]["xlabel"] = labels[i]
  return G,tx


def get_small_graph(j: dict, legend):
  k = sorted(map(int, j.keys()))
  def identify(dct): return (dct["act_id"])
  cnt = 0
  lis = []
  gr = {}
  labels = {}
  for i in k:
    lis.append(j[str(i)]["act_id"])
    if lis[-1] not in gr:
      nex = len(gr)
      labels[nex] = lis[-1]
      gr[lis[-1]] = nex
  # labels, legend = pfxelim(labels)
  labels = {i: legend[labels[i]] for i in labels}
  G = genGraph([(gr[lis[i]], gr[lis[i + 1]]) for i in range(len(lis) - 1)])
  return (G, labels)


def get_multi_small_graph(js: list[dict], legend):
  gr = {}
  labels = {}
  lises = []
  for j in js:
    k = list(map(str, sorted(map(int, j.keys()))))
    # print("Fn",j[k[0]]["act_id"],j[k[0]]["hash"])
    lis = []
    for i in k:
      lis.append(j[i]["act_id"])
      if lis[-1] not in gr:
        nex = len(gr)
        labels[nex] = lis[-1]
        gr[lis[-1]] = nex
    lises += [lis]
  # labels, legend = pfxelim(labels)
  labels = {i: legend[labels[i]] for i in labels}

  G = genGraph([(gr[lis[i]], gr[lis[i + 1]])
               for lis in lises for i in range(len(lis) - 1)])
  return (G, labels)


def cack(G: nx.DiGraph):
  Gco = nx.DiGraph(G)
  for (u,v) in G.edges:
    Gco.add_edge(v,u)
  d=dict(nx.shortest_path_length(Gco)) # type: ignore
  return d

def cacker(G:nx.DiGraph, G2:nx.DiGraph):
  s1 = cack(G)
  s2 = cack(G2)
  ar1 = []
  ar2 = []
  for i in G.nodes:
    for j in G.nodes:
      if G.nodes[i]["color"] == G2.nodes[j]["color"]:
        assert G2.nodes[i]["color"] == G2.nodes[j]["color"]
        assert s1[i][j] <= s2[i][j]
        ar1 += [s1[i][j]]
        ar2 += [s2[i][j]]
  N = 50
  if len(ar1) >= N:
    import random
    x = list(zip(ar1,ar2))
    v = []
    idx = 0
    for i in range(1000):
      if idx + N > len(x):
        random.shuffle(x)
        idx = 0
      dd = scipy.stats.pearsonr([i[0] for i in x[idx:idx+N]], [i[1] for i in x[idx:idx+N]]).pvalue
      idx += N
      if dd == dd:
        v += [dd]
    return v

def ggggolor(G: nx.DiGraph, runTest=False):
  G2 = nx.DiGraph()
  def func(u, v, dc): return 1 / dc["weight"]
  ff: dict = nx.shortest_path(G, 0, weight=func)  # type: ignore
  if 0 in G.nodes and "xlabel" in G.nodes[0]:
    for i in ff:
      if len(ff[i]) >= 2:
        G2.add_edge(ff[i][-2], ff[i][-1])
        G2.nodes[ff[i][-2]]["xlabel"] = G.nodes[ff[i][-2]]["xlabel"]
        G2.nodes[ff[i][-1]]["xlabel"] = G.nodes[ff[i][-1]]["xlabel"]
    for i in ff:
      if len(ff[i]) >= 2:
        if G2.nodes[ff[i][-2]]["xlabel"] in [G2.nodes[ff[i][-1]]["xlabel"], G2.nodes[ff[i][-1]]["xlabel"] + "_DELETE"]:
          G2.nodes[ff[i][-1]]["xlabel"] += "_DELETE"
    for x in G2.nodes:
      G2.nodes[x]["hidden_label"] = G2.nodes[x]["xlabel"]
      if G2.nodes[x]["xlabel"].endswith("_DELETE"):
        G2.nodes[x]["hidden_label"] = G2.nodes[x]["xlabel"][:-7]
        del G2.nodes[x]["xlabel"]
  else:
    for i in G.nodes:
      G2.add_node(i)
    for i in ff:
      if len(ff[i]) >= 2:
        G2.add_edge(ff[i][-2], ff[i][-1])
  Gx = nx.DiGraph(G)
  for u, v in Gx.edges:
    # Gx[u][v]["weight"] = int(1000000/Gx[u][v]["weight"])
    Gx[u][v]["weight"] = 1
  if len(Gx.nodes) <= 1:
    raise Exception
    # return (G, G2)
  # Gx.graph["edge_weight_attr"] = "weight"
  # Gm = metis.networkx_to_metis(Gx)
  # (cut, parts) = metis.part_graph(Gm, 6)
  # colors = ['red', 'blue', '#b5e51d', 'cyan', 'magenta', '#fea200']
  # for i, part in enumerate(parts):
  #   G.nodes[i]['color'] = colors[part]
  #   G2.nodes[i]['color'] = colors[part]
  #   G.nodes[i]['node_value'] = part
  # if len(G.nodes) > 10 and runTest:
  #   s = cacker(G, G2)
  #   if s:
  #     print(sum(s)/len(s))
  return (G, G2)


def checknum(G: nx.DiGraph):
  ans1, ans2 = 0, 0
  for u, v in G.edges:
    if G.nodes[u]["node_value"] == G.nodes[v]["node_value"]:
      ans1 += G[u][v]["weight"]
      ans2 += G[u][v]["weight"]**2
  return (ans1, ans2)


ALL = ["accuweather", "autoscout24", "duolingo", "onenote", "wattpadfreebooks"]
WATTPAD = ["wattpadfreebooks"]
TRIPADVISOR = ["tripadvisor"]

APPNAMES = """accuweather
autoscout24
dictionarymerriamwebster
duolingo
flipboard
linewebtoon
nikerunclub
onenote
quizlet
spotify
tripadvisor
trivago
wattpadfreebooks
wish
youtube
zedge""".split("\n")
# APPNAMES = ["nikerunclub"]
TESTINGTOOLS = ["ape", "monkey", "wctester"]
mm = pickle.load(open("dumpfile.pickle","rb"))
fx = []
fy = []
for x in APPNAMES:
  print(x)
  actvs = []
  for tt in TESTINGTOOLS:
    js = []
    PFX = f"{tt}-{x}"
    for y in range(1, 7):
      DIR = f"{PFX}-{y}"
      j = mm[DIR]
      actvs += [j[t]["act_id"] for t in j]
  legend = pfxelim(set(actvs))
  dt:dict[str,list] = {i:[None]*7 for i in TESTINGTOOLS}
  jn = {}
  DATv = {}
  DSTv = {}
  DCATv = {}
  DATvG = {}
  DSTvG = {}
  DCATvG = {}
  for tt in TESTINGTOOLS:
    PFX = f"{tt}-{x}"
    js = []
    DATv[tt] = {i:[] for i in legend}
    DSTv[tt] = {i:[] for i in legend}
    DATvG[tt] = {i:[] for i in legend}
    DSTvG[tt] = {i:[] for i in legend}
    for y in range(1,7):
      DIR = f"{PFX}-{y}"
      j = mm[DIR]
      js += [j]
      try:
        G, tx = get_big_graph(j, DIR, legend)
        print(DIR)
        (G, G2) = ggggolor(G,True)
        (Gr, labels) = get_small_graph(j, legend)
        (Gr, Gr2) = ggggolor(Gr)
        Gr2 = nx.relabel_nodes(Gr2, labels)
        Gr3:nx.DiGraph = nx.relabel_nodes(Gr, labels)
        dt[tt][y] = (tx/60000,len(G.nodes),len(Gr.nodes))
        bee = nx.shortest_path_length(G2, 0)
        for i in legend:
          re = []
          for jidx, n in G2.nodes.items():
            if n["hidden_label"] == legend[i]:
              re += [bee[jidx]]
          if re:
            DSTv[tt][i] += [sum(re)/len(re)]
          if legend[i] in Gr2.nodes:
            DATv[tt][i] += [nx.shortest_path_length(Gr2, labels[0], legend[i])]
        bee = nx.shortest_path_length(G, 0)
        for i in legend:
          re = []
          for jidx, n in G.nodes.items():
            if n["xlabel"] == legend[i]:
              re += [bee[jidx]]
          if re:
            DSTvG[tt][i] += [sum(re)/len(re)]
          if legend[i] in Gr3.nodes:
            DATvG[tt][i] += [nx.shortest_path_length(Gr3, labels[0], legend[i])]
      except Exception as e:
        import traceback
        # traceback.print_exc()
        print("fail", DIR)
        pass
    jn[tt] = generate_legend_cumulative(legend, PFX, js)
    (Gr, labels) = get_multi_small_graph(js, legend)
    try:
      (Gr, Gr2) = ggggolor(Gr)
    except:
      print("utter failure", PFX)
      DCATv[tt] = {}
      DCATvG[tt] = {}
      continue
    Gr2:nx.DiGraph = Gr2
    Gr2 = nx.relabel_nodes(Gr2, labels)
    Gr = nx.relabel_nodes(Gr, labels)
    DCATv[tt] = {i:nx.shortest_path_length(Gr2, labels[0], legend[i]) for i in legend if legend[i] in Gr2.nodes}
    DCATvG[tt] = {i:nx.shortest_path_length(Gr, labels[0], legend[i]) for i in legend if legend[i] in Gr.nodes}
    Gr3: nx.DiGraph = nx.relabel_nodes(Gr, labels)
    # print(checknum(Gr))
  with open(f"tables/summary-{x}.tex","w") as f:
    # print(r"\subsection{"+x+"}",file=f)
    print(r"\begin{tabular}{llll}",file=f)
    print("&Ape testing tool&Monkey testing tool&WCTester testing tool\\\\",file=f)
    for tt in TESTINGTOOLS:
      print("&\\hyperref[subsubsec:%s-%s]{"%(x,tt)+"Summary}",file=f)
    print("\\\\",file=f)
    for i in range(1,7):
      print("Run",i,file=f)
      for tt in TESTINGTOOLS:
        if dt[tt][i] is None:
          print("&Failed",file=f)
        else:
          y = dt[tt][i]
          print(r"&\hyperref[subsubsec:"+"%s-%s-%d]{%.1fmin (%d/%d nodes)}" % (x, tt, i, y[0], y[1], y[2]),file=f)
      print("\\\\", file=f)
    print(r"\end{tabular}",file=f)
    print(file=f)
    print(r"""\noindent%
\begin{picture}(\textwidth,0.4\textheight)%
  \put(0,0){
    \begin{minipage}{\textwidth}
      \resizebox{\textwidth}{!}{\input{tables/vis-"""+x+r""".tex}}
    \end{minipage}
  }
\end{picture}""",file=f)

    with open(f"tables/vis-{x}.tex","w") as f2:
      CNT =  9
      dd = partialpfxelim(set(legend))
      spec = r"llll@{\hskip 3pt}ll@{\hskip 3pt}ll@{\hskip 3pt}l"
      assert spec.count("l") == CNT
      print(r"\begin{tabular}{|l|" + (spec+'|')*len(TESTINGTOOLS) + "}",file=f2)
      print(r"\hline",file=f2)
      print(r"Legend",file=f2)
      mcd = "& \\multicolumn{"+str(CNT)+"}"
      for tt in TESTINGTOOLS:
        cnt = sum(1 for i in legend if jn[tt][i][1])
        print(mcd+r"{l|}{"+tt+" (%d activities)}"%cnt,file=f2)
      print("\\\\ \\cline{2-%d}" % (1+len(TESTINGTOOLS)*CNT),file=f2)
      print(r"\\[-9.3pt]",file=f2)
      for tt in TESTINGTOOLS:
        b=r"&$\overline{T_{\text{A}}}$ &$\overline{G_{\text{A}}}$"
        b+=r"&$\overline{T_{\text{S}}}$ &$\overline{G_{\text{S}}}$"
        b+=r"& $T_{\text{C}}$& $G_{\text{C}}$"
        print(r"& \# (\%) & $\overline{\mbox{ms}}$ (\%) & $\Sigma_R$",b,file=f2)
      print(r"\\ \hline",file=f2)
      DATvR = {}
      DATvGR = {}
      DSTvR = {}
      DSTvGR = {}
      DCATvR = {}
      DCATvGR = {}
      for tt in TESTINGTOOLS:
        DATvR[tt] = []
        DATvGR[tt] = []
        DSTvR[tt] = []
        DSTvGR[tt] = []
        DCATvR[tt] = []
        DCATvGR[tt] = []
        for i in DATv[tt]:
          if len(DATv[tt][i]):
            DATv[tt][i] = sum(DATv[tt][i]) / len(DATv[tt][i])
            DATvR[tt] += [DATv[tt][i]]
        for i in DATvG[tt]:
          if len(DATvG[tt][i]):
            DATvG[tt][i] = sum(DATvG[tt][i]) / len(DATvG[tt][i])
            DATvGR[tt] += [DATvG[tt][i]]
        for i in DSTv[tt]:
          if len(DSTv[tt][i]):
            DSTv[tt][i] = sum(DSTv[tt][i]) / len(DSTv[tt][i])
            DSTvR[tt] += [DSTv[tt][i]]
        for i in DSTvG[tt]:
          if len(DSTvG[tt][i]):
            DSTvG[tt][i] = sum(DSTvG[tt][i]) / len(DSTvG[tt][i])
            DSTvGR[tt] += [DSTvG[tt][i]]
        for i in DCATv[tt]:
          DCATvR[tt] += [DCATv[tt][i]]
        for i in DCATvG[tt]:
          DCATvGR[tt] += [DCATvG[tt][i]]
        try:
          fx += DATvR[tt]+DSTvR[tt]+DCATvR[tt]
          fy += DATvGR[tt]+DSTvGR[tt]+DCATvGR[tt]
        except:
          pass
      for li, i in sorted((legend[a],a) for a in legend):
        print(dd[i].replace("_","\\_"),file=f2)
        for tt in TESTINGTOOLS:
          re = jn[tt][i]
          if re[1] and DATv[tt][i] != []:
            def d(x,r="%.1f",tr=True):
              s = r%x
              a, b = s.split(".")
              if len(a) >= 2:
                return a
              return a+"."+r"{\footnotesize"+b+"}"
            re += ("%s & %s & %s & %s & %d &%d" % (d(DATv[tt][i]), d(DATvG[tt][i]), 
                                         d(DSTv[tt][i]), d(DSTvG[tt][i]), 
                                         DCATv[tt][i], DCATvG[tt][i]),)
          else:
            re += ("&&&&&",)
          assert ("&".join(re)).count("&")==CNT-1
          # assert len(re) == CNT
          print(" & " + " & ".join(re),file=f2)
        print(r"\\",file=f2)
      print(r"\hline\end{tabular}",file=f2)
    print(r"\pagebreak{}",file=f)
    for tt in TESTINGTOOLS:

      print("\\subsubsection*{%s-%s Summary}" % (x,tt), file=f)
      print("\\label{"+"subsubsec:%s-%s}"%(x,tt),file=f)
      print(r"\begin{center}\small",file=f)
      print(r"\hyperref[subsec:"+x+"]{Section}",file=f,end="")
      for y in TESTINGTOOLS:
        if y == tt:
          print(r" $\cdot$ \underline{"+y+"}",file=f,end="")
        else:
          print(r" $\cdot$ \hyperref[subsubsec:"+x+"-"+y+"]{"+y+"}",file=f,end="")
      for y in range(1,7):
        print(r" $\cdot$ \hyperref[subsubsec:"+"%s-%s-%d]{Run %d}"%(x,tt,y,y),file=f,end="")
      print(r"\end{center}",file=f)
      print("\\generatesummaryvisualization{%s-%s}"%(tt,x),file=f)
      print(r"\pagebreak",file=f)

      for i in range(1,7):
        if dt[tt][i] is not None:
          print("\\subsubsection*{%s-%s-%d}" % (x,tt,i),file=f)
          print("\\label{"+"subsubsec:%s-%s-%d}" % (x,tt,i),file=f)
          print(r"\begin{center}\small",file=f)
          print(r"\hyperref[subsec:"+x+"]{Section}",file=f,end="")
          print(r" $\cdot$ \hyperref[subsubsec:"+"%s-%s]{%s}"%(x,tt,x),file=f,end="")
          for y in range(1,7):
            if y == i:
              print(r" $\cdot$ \underline{Run "+str(y)+"}",file=f,end="")
            else:
              print(r" $\cdot$ \hyperref[subsubsec:"+"%s-%s-%d]{Run %d}"%(x,tt,y,y),file=f,end="")
          print(r"\end{center}",file=f)
          print("\\generaterunvisualization{%s-%s-%d}" % (tt,x,i),file=f)
          print(r"\pagebreak",file=f)

print(scipy.stats.pearsonr(fx,fy))